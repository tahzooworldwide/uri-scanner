# URI Scanner

Node.js based tool to scan URIs and report status and duration

---

## Setup

*This package requires node.js to be installed on your local machine, please install node.js before proceeding.*

1. Clone or Download & Extract a copy of this repositiory.
2. In a terminal, navigate to the resulting folder from the previous step.
3. Install dependencies by running, `npm install`

You are now ready to add the URIs to be scanned.

---

## Adding Seeds

In a text editor, open the file named `seeds.txt`

Add the URIs to be scanned to the file, one per line.

Example: 

```text
https://tahzoo.com
https://google.com
https://bing.com
```

Save this file. You are now ready to run the scanner.

---

## Scan URIs

### Starting the Scan Process

To execute the scanner, execute the following command in your terminal window.

```bash
# Run the scan (do not include the $, it represents the terminal prompt)
$ node scan.js
```

This will print the results to the terminal window.

### Stopping the Scan Process

To stop the scan, type `Ctrl+c`

---

## Custom Configuration

*Warning: Unless you possess The Holy Hand Grenade of Antioch, you might want to ignore this*

The scanner checks for an external configuration file, `scan-config.json`, during initialization. You can customize the default settings for the scanner in this file.

Sample `scan-config.json`:

```JSON
{
  "delay": 1000,
  "dryRun": false,
  "inputFilename": "seeds.txt",
  "outputFormat": "console",
  "printCsvHeadings": false
}
```

Config Options:

|Option|Default|Description|
|---|---|---|
|`delay`|`1000`|Delay between calls in milliseconds, suggested sane minimum: `250`|
|`dryRun`|`false`|When true, print URI value only|
|`inputFilename`|`seeds.txt`|Input Filname|
|`outputFormat`|`console`|Output format: `console`, `csv`,|
|`printCsvHeadings`|`false`|Print CSV Headings, only applicable when `outputFormat` is `csv`|


### Example config for CSV output

`scan-config.json`:

```JSON
{
  "outputFormat": "csv",
  "printCsvHeadings": true
}
```

Run and capture output

```
$ node scan.js > output.csv
```


