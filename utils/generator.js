/**
 * Generator function that loops though a set of URIs
 * @param {Array} uris Array of URIs
 */
module.exports =  async function* generator(uris, delay) {
  let index = 0;
  while (true) {
    await new Promise(resolve => setTimeout(resolve, delay));
    yield uris[index];
    index = (index + 1) % uris.length;
  }
}