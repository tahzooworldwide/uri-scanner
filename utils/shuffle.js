/**
* Knuth shuffle (Fisher-Yates) to randomize seed order
* ("Everyday I'm Shufflin'" - LMFAO)
* 
* @param {Array} array 
* @returns Array of shuffled entries
*/
module.exports = function shuffle(array) {
 let currentIndex = array.length;
 let randomIndex;

 // While there remain elements to shuffle...
 while (0 !== currentIndex) {

   // Pick a remaining element...
   randomIndex = Math.floor(Math.random() * currentIndex);
   currentIndex--;

   // And swap it with the current element.
   [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
 }

 return array;
}
