const fs = require('fs');
const fetch = require('node-fetch');
const shuffle = require('./utils/shuffle');
const generator = require('./utils/generator');

// Configuration
const CONFIG_FILE = './scan-config.json';

const CONFIG_DEFUALTS = {
  delay: 1000,                  // Delay between calls in milliseconds, suggested sane minimum: 250
  dryRun: false,                // When true, print URI value only
  inputFilename: 'seeds.txt',   // Input Filname
  outputFormat: 'console',      // Output format: console, csv,
  printCsvHeadings: false,      // Print CSV Headings, only applicable when outputFormat is csv
}

/**
 * Get Configuration
 * Merge defaults with external config JSON
 * 
 * @returns Merged configuration object
 */
function getConfig() {
  let externalConfig = {}
  if (fs.existsSync(CONFIG_FILE)) externalConfig = JSON.parse(fs.readFileSync(CONFIG_FILE, 'utf8'));
  return { ...CONFIG_DEFUALTS, ...externalConfig };
}

// Mock function for dev/testing
function mock(uri) {
  console.log(uri);
}


/**
 * Fetch inputed URI and return timestamped result
 * 
 * @param {String} uri URI to fetch 
 */
function scan(uri, output) {
  // Set Start Time for duration
  const timerStart = new Date().getTime();

  // Execute request and report
  fetch(uri)
    .then(response => {

      // Calculate duration
      const timeEllapsed = new Date().getTime() - timerStart;

      // Output result
      if (output === 'csv') {
        console.log(`${new Date().toLocaleString()}, ${response.status}, ${response.statusText}, ${timeEllapsed}, ${response.url}`)
      } else {
        console.log(`[${new Date().toLocaleString()}] ${response.status} :: ${response.statusText} -- ${response.url} (${timeEllapsed} ms)`)
      }
    });
}

/** Main function */
async function main() {
  // Get Configuration
  const cfg = getConfig();

  // Read URIs from seed file and shuffle
  const uris = shuffle(fs.readFileSync(cfg.inputFilename).toString().split("\n"));

  // Print headers for CSV
  if (cfg.outputFormat === 'csv' && cfg.printCsvHeadings) {
    console.log(`Date, Time, Status, Status Text, Duration(ms), Target`)
  }

  // Execute
  for await (let uri of generator(uris, cfg.delay)) {
    if (cfg.dryRun) {
      mock(uri);
    } else {
      scan(uri, cfg.outputFormat);
    }
  }
}

// Run
main();
